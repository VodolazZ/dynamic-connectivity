using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DynamicConnectivity;

namespace UnitTests.Framework
{
    internal class InMemoryEdgesProvider : IEdgesProvider
    {
        private readonly List<GraphEdge> _generatedData;

        public InMemoryEdgesProvider(int edgesNumber, int disjointedSubsetsCount = 1)
        {
            if (edgesNumber < 1 || disjointedSubsetsCount < 1)
            {
                throw new ArgumentException("test error: provide edges and sets count > 0");
            }
            
            _generatedData = new List<GraphEdge>(edgesNumber);
            var dataBySubsets = new List<List<GraphEdge>>(disjointedSubsetsCount);
            
            for (int subsetsGenCount = 0; subsetsGenCount < disjointedSubsetsCount; subsetsGenCount++)
            {
                var initialEdge = new GraphEdge{NodeA = Guid.NewGuid().ToString(), NodeB = Guid.NewGuid().ToString()};
                dataBySubsets.Add(new List<GraphEdge> {initialEdge});
            }
            
            var rnd = new Random();
            for (int generatorCount = 0; generatorCount < edgesNumber; generatorCount++)
            {
                var acquiringSubsetId = rnd.Next(0, disjointedSubsetsCount);
                var connectedEdgeId = rnd.Next(0, dataBySubsets[acquiringSubsetId].Count);
                
                var nextEdge = new GraphEdge{NodeA = dataBySubsets[acquiringSubsetId][connectedEdgeId].NodeB, 
                                             NodeB = Guid.NewGuid().ToString()};
                
                _generatedData.Add(nextEdge);
                dataBySubsets[acquiringSubsetId].Add(nextEdge);
            }

            _generatedData = _generatedData.OrderBy(_ => rnd.Next()).ToList();
        }

        public IEnumerator<GraphEdge> GetEnumerator()
        {
            return _generatedData.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void Dispose()
        {
        }
    }
}