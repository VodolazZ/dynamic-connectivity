using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DynamicConnectivity;
using DynamicConnectivity.CsvReader;
using UnitTests.Framework;
using Xunit;
// ReSharper disable MemberCanBePrivate.Global

namespace UnitTests
{
    public class CsvReaderTests
    {
        [Fact]
        internal void Constructor_OnWrongFileName_ThrowsException()
        {
            var fileName = Guid.NewGuid().ToString();

            Assert.Throws<FileNotFoundException>(() => new EdgesCsvProvider(fileName));
        }

        [Fact]
        internal void Enumerator_OnEmptyFile_ReturnsNoElements()
        {
            var fileName = Constants.CsvFilesPaths.EMPTY_FILE_PATH;
            var csvProvider = new EdgesCsvProvider(fileName);

            using (var enumerator = csvProvider.GetEnumerator())
            {
                Assert.False(enumerator.MoveNext());
            }
        }
        
        [Fact]
        internal void Enumerator_OnFileWithOnlyHeader_ReturnsNoElements()
        {
            var fileName = Constants.CsvFilesPaths.ONLY_HEADER_FILE_PATH;
            var csvProvider = new EdgesCsvProvider(fileName);

            using (var enumerator = csvProvider.GetEnumerator())
            {
                Assert.False(enumerator.MoveNext());
            }
        }

        [Theory]
        [MemberData(nameof(NonEmptyCsvMemberData))]
        internal void Enumerator_OnGoodCsvWithData_ReturnsCorrectNumberOfElements(
            EdgesCsvProvider provider,
            int numberOfRecords)
        {
            Assert.Equal(numberOfRecords, provider.Count());
        }

        public static IEnumerable<object[]> NonEmptyCsvMemberData()
        {
            yield return new object[] {new EdgesCsvProvider(Constants.CsvFilesPaths.GOOD1_FILE_PATH), 3};
            yield return new object[] {new EdgesCsvProvider(Constants.CsvFilesPaths.GOOD2_FILE_PATH), 4};
        }
    }
}