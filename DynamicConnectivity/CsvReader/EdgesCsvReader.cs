using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace DynamicConnectivity.CsvReader
{
    internal class EdgesCsvProvider : IEdgesProvider
    {
        private readonly string _filePath;
        
        public EdgesCsvProvider(string filePath)
        {
            _filePath = File.Exists(filePath) ? filePath : throw new FileNotFoundException(nameof(filePath));
        }

        public IEnumerator<GraphEdge> GetEnumerator()
        {
            var csvReader = new CsvHelper.CsvReader(new StreamReader(_filePath));
            csvReader.Configuration.HasHeaderRecord = true;
            csvReader.Configuration.CultureInfo = CultureInfo.InvariantCulture;

            return new EdgesCsvEnumerator(csvReader);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void Dispose()
        {
        }
    }
}