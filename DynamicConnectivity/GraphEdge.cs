using CsvHelper.Configuration.Attributes;

namespace DynamicConnectivity
{
    internal class GraphEdge
    {
        [Index(0)]
        public string NodeA { get; set; }
        
        [Index(1)]
        public string NodeB { get; set; }
    }
}