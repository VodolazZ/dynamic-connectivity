﻿using System;
using DynamicConnectivity.CsvReader;

namespace DynamicConnectivity
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter path to .csv file:");
            var path = Console.ReadLine();

            var edgesProvider = new EdgesCsvProvider(path);
            var graphExplorer = new GraphExplorer(edgesProvider);
            
            Console.WriteLine($"graph explorer found it is {graphExplorer.CheckAllNodesConnected()} that all nodes were connected");
            Console.ReadLine();
        }
    }
}