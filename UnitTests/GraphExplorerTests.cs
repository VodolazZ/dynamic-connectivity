using System.Collections.Generic;
using DynamicConnectivity;
using DynamicConnectivity.CsvReader;
using UnitTests.Framework;
using Xunit;

namespace UnitTests
{
    public sealed class GraphExplorerTests
    {
        [Theory]
        [MemberData(nameof(PositiveScenarioEdgesTestData))]
        internal void CheckAllNodesConnected_ReturnCorrectValueForInputScenario(IEdgesProvider edgesProvider, bool isEverythingConnected)
        {
            var graphExplorer = new GraphExplorer(edgesProvider); 
            
            Assert.Equal(isEverythingConnected, graphExplorer.CheckAllNodesConnected());
        }

        public static IEnumerable<object[]> PositiveScenarioEdgesTestData()
        {
            yield return new object[] {new EdgesCsvProvider(Constants.CsvFilesPaths.LogicTests.First.PATH), Constants.CsvFilesPaths.LogicTests.First.IS_ONE_SEGMENT};
            yield return new object[] {new EdgesCsvProvider(Constants.CsvFilesPaths.LogicTests.Second.PATH), Constants.CsvFilesPaths.LogicTests.Second.IS_ONE_SEGMENT};
        }
    }
}