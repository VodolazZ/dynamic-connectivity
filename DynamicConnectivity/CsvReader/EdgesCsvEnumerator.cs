using System.Collections;
using System.Collections.Generic;

namespace DynamicConnectivity.CsvReader
{
    internal class EdgesCsvEnumerator : IEnumerator<GraphEdge>
    {
        private readonly IEnumerator<GraphEdge> _innerEnumerator;
        private readonly GraphEdge _iterationElement;
        private readonly CsvHelper.CsvReader _reader;
        
        public EdgesCsvEnumerator(CsvHelper.CsvReader reader)
        {
            _reader = reader;
            _iterationElement = new GraphEdge();
            _innerEnumerator = reader.EnumerateRecords(_iterationElement).GetEnumerator();
        }
        
        public bool MoveNext()
        {
            return _innerEnumerator.MoveNext();
        }

        public void Reset()
        {
            _innerEnumerator.Reset();
        }

        public GraphEdge Current => _iterationElement;

        object IEnumerator.Current => Current;

        public void Dispose()
        {
            _innerEnumerator.Dispose();
            _reader.Dispose();
        }
    }
}