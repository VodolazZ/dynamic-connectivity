namespace DynamicConnectivity
{
    internal sealed class GraphExplorer
    {
        private readonly DisjointSet _set;
        private readonly IEdgesProvider _edgesProvider;
        private bool _setWasBuilt;
        
        public GraphExplorer(IEdgesProvider edgesProvider, string rootElement = null)
        {
            _set = new DisjointSet(rootElement);
            _edgesProvider = edgesProvider;
        }

        public bool CheckAllNodesConnected()
        {
            if (!_setWasBuilt)
            {
                foreach (var edge in _edgesProvider)            
                {
                    _set.AddEdge(edge);
                }

                _setWasBuilt = true;
            }

            return _set.SubsetsCount == 1;
        }
    }
}