using System;
using System.Collections.Generic;

namespace DynamicConnectivity
{
    internal class DisjointSet
    {
        private readonly List<int> _setsArray = new List<int>();
        private readonly Dictionary<string, int> _nodeValues = new Dictionary<string, int>();
        private readonly Dictionary<int, int> _rootWeights = new Dictionary<int, int>();
        
        public DisjointSet(string rootNode = null)
        {
            if (rootNode != null)
            {
                this.AddUniqueNode(rootNode);
            }
        }

        public int SubsetsCount { get => _rootWeights.Count; }

        public void AddEdge(GraphEdge graphEdge)
        {
            if (!_nodeValues.ContainsKey(graphEdge.NodeA))
            {
                this.AddUniqueNode(graphEdge.NodeA);
            }

            if (!_nodeValues.ContainsKey(graphEdge.NodeB))
            {
                this.AddUniqueNode(graphEdge.NodeB);
            }

            this.AddEdgeBetweenExistingNodes(graphEdge.NodeA, graphEdge.NodeB);
        }

        private void AddEdgeBetweenExistingNodes(string edgeNodeA, string edgeNodeB)
        {
            var nodeARootIndex = this.FindRoot(edgeNodeA);
            var nodeBRootIndex = this.FindRoot(edgeNodeB);

            if (nodeARootIndex != nodeBRootIndex)
            {
                var weightRootA = _rootWeights[nodeARootIndex];
                var weightRootB = _rootWeights[nodeBRootIndex];

                var acquiringRootNode = weightRootA      >= weightRootB ? nodeARootIndex : nodeBRootIndex;
                var acquiredRootNode = acquiringRootNode == nodeBRootIndex ? nodeARootIndex : nodeBRootIndex;

                _setsArray[acquiredRootNode] = acquiringRootNode;
                _rootWeights[acquiringRootNode] += _rootWeights[acquiredRootNode];
                _rootWeights.Remove(acquiredRootNode);
            }
        }

        private int FindRoot(string node)
        {
            var currentElementIndex = _nodeValues[node];
            while (_setsArray[currentElementIndex] != currentElementIndex)
            {
                _setsArray[currentElementIndex] = _setsArray[_setsArray[currentElementIndex]];
                currentElementIndex = _setsArray[currentElementIndex];
            }

            _setsArray[_nodeValues[node]] = currentElementIndex;
            
            return currentElementIndex;
        }

        private void AddUniqueNode(string nodeToAdd)
        {
            _nodeValues.Add(nodeToAdd, _setsArray.Count);
            _rootWeights.Add(_setsArray.Count, 1);
            _setsArray.Add(_setsArray.Count);
        }
    }
}