namespace UnitTests.Framework
{
    public static class Constants
    {
        public static class CsvFilesPaths
        {
            private const string FILE_PATH = "TestCsvFiles/";

            public const string EMPTY_FILE_PATH = FILE_PATH + "empty.csv";

            public const string ONLY_HEADER_FILE_PATH = FILE_PATH + "only_header.csv";
            
            public const string GOOD1_FILE_PATH = FILE_PATH + "good1.csv";
            
            public const string GOOD2_FILE_PATH = FILE_PATH + "good2.csv";

            public static class LogicTests
            {
                private const string LOGIC_FILE_PATH = FILE_PATH + "Logic/";

                public static class First
                {
                    public const string PATH = LOGIC_FILE_PATH + "1.csv";
                    public const bool IS_ONE_SEGMENT = true;
                }
                
                public static class Second
                {
                    public const string PATH = LOGIC_FILE_PATH + "2.csv";
                    public const bool IS_ONE_SEGMENT = false;
                }
            }
        }
    }
}