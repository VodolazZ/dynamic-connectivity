using System;
using System.Collections.Generic;

namespace DynamicConnectivity
{
    internal interface IEdgesProvider : IEnumerable<GraphEdge>, IDisposable
    {
    }
}