using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using DynamicConnectivity;
using UnitTests.Framework;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests
{
    public class DisjointSetTests
    {
        private readonly ITestOutputHelper _testOutput;
        
        public DisjointSetTests(ITestOutputHelper output)
        {
            _testOutput = output;
        }
        
        [Fact]
        internal void Set_OnCreation_IsEmpty()
        {
            var disjointSet = new DisjointSet();
            
            Assert.Equal(0, disjointSet.SubsetsCount);
        }

        [Fact]
        internal void Set_OnCreationWithRootItem_ContainsOneSubset()
        {
            var testNode = "test_node";
            var disjointSet = new DisjointSet(testNode);
            
            Assert.Equal(1, disjointSet.SubsetsCount);
        }

        [Fact]
        internal void Set_AddEdge_DoesNotIncrementSubsetCountWhenConnectedWithExistingSubset()
        {
            var rootNode = "root_node";
            var unlinkedEdge = new GraphEdge()
            {
                NodeA = new String(rootNode),
                NodeB = "b_node"
            };
            
            var disjointSet = new DisjointSet(rootNode);
            disjointSet.AddEdge(unlinkedEdge);
            
            Assert.Equal(1, disjointSet.SubsetsCount);
        }
        
        [Fact]
        internal void Set_AddEdge_IncrementSubsetCountWhenNotConnectedWithExistingSubset()
        {
            var rootNode = "root_node";
            var unlinkedEdge = new GraphEdge()
            {
                NodeA = "a_node",
                NodeB = "b_node"
            };
            
            var disjointSet = new DisjointSet(rootNode);
            disjointSet.AddEdge(unlinkedEdge);
            
            Assert.Equal(2, actual: disjointSet.SubsetsCount);
        }

        [Theory]
        [MemberData(nameof(PositiveScenarioEdgesTestData))]
        internal void Set_SubsetsCount_ReturnCorrectNumberForGoodScenario(IEdgesProvider edgesProvider, int disjointedSetsCount)
        {
            var set = new DisjointSet();

            foreach (var edge in edgesProvider)
            {
                set.AddEdge(edge);
            }
            
            Assert.Equal(disjointedSetsCount, set.SubsetsCount);
        }

        public static IEnumerable<object[]> PositiveScenarioEdgesTestData()
        {
            var disjointedSetsCountCase1 = 1;
            yield return new object[] {new InMemoryEdgesProvider(10, disjointedSetsCountCase1), disjointedSetsCountCase1};
            
            var disjointedSetsCountCase2 = 2;
            yield return new object[] {new InMemoryEdgesProvider(20, disjointedSetsCountCase2), disjointedSetsCountCase2 };
            
            var disjointedSetsCountCase3 = 3;
            yield return new object[] {new InMemoryEdgesProvider(50, disjointedSetsCountCase3), disjointedSetsCountCase3 };
            
            var disjointedSetsCountCase4 = 4;
            yield return new object[] {new InMemoryEdgesProvider(100, disjointedSetsCountCase4), disjointedSetsCountCase4 };
        }
        
        [Theory]
        [MemberData(nameof(EnsureHandlingHugeEdgesAmountTestData))]
        internal void Set_SubsetsCount_EnsureHugeAmountsHandling(IEdgesProvider edgesProvider, int count)
        {
            var set = new DisjointSet();

            var sw = new Stopwatch();
            sw.Start();
            foreach (var edge in edgesProvider)
            {
                set.AddEdge(edge);
            }
            sw.Stop();
            
            Assert.Equal(1, actual: set.SubsetsCount);
            _testOutput.WriteLine($"took {sw.Elapsed.Milliseconds} to handle {count} items");
        }

        public static IEnumerable<object[]> EnsureHandlingHugeEdgesAmountTestData()
        {
            var edgesNumber1 = 100;
            yield return new object[] {new InMemoryEdgesProvider(edgesNumber1, 1), edgesNumber1};
            
            var edgesNumber2 = 1_000;
            yield return new object[] {new InMemoryEdgesProvider(edgesNumber2, 1), edgesNumber2};
            
            var edgesNumber3 = 10_000;
            yield return new object[] {new InMemoryEdgesProvider(edgesNumber3, 1), edgesNumber3};
            
            var edgesNumber4 = 100_000;
            yield return new object[] {new InMemoryEdgesProvider(edgesNumber4, 1), edgesNumber4};
            
            var edgesNumber5 = 1_000_000;
            yield return new object[] {new InMemoryEdgesProvider(edgesNumber5, 1), edgesNumber5};
            
            // this is actually last power of 10 we can handle using Guid(36 byte-string) as a node name.
            // for next order of magnitude 32bit process memory wont'be enough (4b + 36b)*100m 
            var edgesNumber6 = 10_000_000;
            yield return new object[] {new InMemoryEdgesProvider(edgesNumber6, 1), edgesNumber6};
        }
    }
}